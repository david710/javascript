/*
Primitivos (imutáveis) - string, number, boolean, undefined, 
null (bigint, symbol) - Valores copiados

Referência (mutável) - array, object, function - Passados por referência

Atribuições em array e object são passados por referência:

let a = [1, 2, 3];
let b = a; Não háa cópia apenas referência. Alterar b altera a

let b = [...a]; //copia o valor do array a
const b = {...a}; //copia o objeto a para b

*/
const a = {
  nome: 'Luiz',
  sobrenome: 'Otávio'
};
const b = a;

b.nome = 'João';
console.log(a);
console.log(b);
