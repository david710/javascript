/*function saudacao (){
    console.log('Bom dia!');
}*/

/*
function saudacao (nome){
    console.log(`Bom dia ${nome}!`);
}

saudacao('Luiz');
saudacao('Maria');
saudacao('Felipe');



function saudacao (nome){
    console.log(`Bom dia ${nome}!`);
    return 123456;
}


function saudacao (nome){
    return `Bom dia ${nome}!`;
}

const variavel = saudacao('Luiz');
console.log(variavel);



function soma(x, y){
    const resultado = x + y;
    return resultado;
    //console.log('Ola Mundo'); Nada é executado apó o return
}

const resultado = soma(2,2);
console.log(resultado);



const raiz = function (n) {
    return n ** 0.5;
};

console.log(raiz(9));
console.log(raiz(16));
console.log(raiz(25));



const raiz = (n) => {
    return n ** 0.5;
};

console.log(raiz(9));
console.log(raiz(16));
console.log(raiz(25));

*/

const raiz = n => n ** 0.5; //Mesma coisa que a anterior (arrow function)

console.log(raiz(9));
console.log(raiz(16));
console.log(raiz(25));

/*
O ideal é apenas criar uma função pequena que cumpra um papel específico.

*/