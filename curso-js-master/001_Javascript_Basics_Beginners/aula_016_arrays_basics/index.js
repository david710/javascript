//               0       1        2          3      4
//let alunos = ['Luiz', 'Maria', 'João']; // Luiza, Eduardo 
//alunos = 123;

const alunos = ['Luiz', 'Maria', 'João']

console.log(typeof alunos);
console.log(alunos instanceof Array);

alunos.push('Luiza');
alunos.push('Eduardo');

console.log(alunos)

console.log(alunos.slice(0, 2));

// console.log(alunos(50)) / por não existir esse indice dará undefined

// alunos.pop() /Remove elemento do fim do array
// alunos.shift() /Remove elemento do começo do array
// alunos.push('Luiza');
// alunos.push('Eduardo');

// console.log(alunos.slice(0, -2));

// console.log(alunos[50]);

// delete alunos[1]; / remove o elemento mantendo a posição vazia
// console.log(alunos[1]);

// const removido = alunos.shift();
// console.log(removido);
// console.log(alunos);

// alunos.unshift('Fábio'); // Adiciona no começo
// alunos.unshift('Luiza');


// alunos.push('Luiza'); // Adiciona no fim
// alunos.push('Fábio');

// alunos[alunos.length] = 'Luiza';  // Adiciona no fim
// alunos[alunos.length] = 'Fábio';
// alunos[alunos.length] = 'Luana';

// alunos[0] = 'Eduardo'; // Altera
// alunos[3] = 'Luiza'; // Adicionando

// console.log(alunos);
// console.log(alunos[0]);
// console.log(alunos[2]);
