/*
&& -> false && true -> false "o valor mesmo"
|| -> true && false -> vai retornar "o valor verdadeiro"

FALSY
*false
0
'' "" ``
null / undefined
NaN
*/

//console.log('Luiz Otávio' && 0 && 'Maria');
//console.log('Luiz Otávio' && true && NaN);

//console.log('Luiz' && 'Maria');

//console.log('Luiz' && NaN && 'Maria');

/*
function falaOi (){
    return 'Oi';
};

const vaiExecutar = false;

console.log(vaiExecutar && falaOi());


function falaOi (){
    return 'Oi';
};

let vaiExecutar;

console.log(vaiExecutar && falaOi());


function falaOi (){
    return 'Oi';
};

let vaiExecutar = 'Joãozinho';

console.log(vaiExecutar && falaOi());


console.log(0 || false || null || 'Luiz Otávio' || true);



const corUsuario = null;
const corPadrao = corUsuario || 'preto';

console.log(corPadrao);
*/

const corUsuario = 'vermelho';
const corPadrao = corUsuario || 'preto';

console.log(corPadrao);