// Valor por referência
//                 0         1        2        3          4
// const nomes = ['Eduardo', 'Maria', 'Joana', 'Wallace', 'Rosana'];
// const novo = nomes.slice(1, -2);
// console.log(novo);

// const nome = 'Luiz Otávio Miranda';
// const nomes = nome.split(' ');
/*
nomes.unshift(); // adiciona no começo
nomes.pop(); // adiciona no final

*/

const nomes = [ 'Luiz', 'Otávio', 'Miranda' ];
const nome = nomes.join(' ');
console.log(nome);
